import React from "react";

class FilmsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: "{  allFilms {  title,   characters {  name gender } } }"
      })
    })
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            items: result.data.allFilms
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <ul>
          {items.map(item => (
            <li key={item.title}>
              <h2>{item.title}</h2>

              {item.characters.map(actor => (
                <li key={actor.name}>
                  Name: {actor.name} / Gender {actor.gender}
                </li>
              ))}
            </li>
          ))}
        </ul>
      );
    }
  }
}

export default FilmsComponent;
