import React from "react";
import ReactDOM from "react-dom";
import FilmsComponent from './films'

import "./styles.css";

function App() {
  return (
    <div className="App">
      <h1>Star Wars Films</h1>
      <FilmsComponent/>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
